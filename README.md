# AOD "Headless" Theme

This is a WordPress Theme designed to be as minimal as possible, it really only includes the index.php file and a functions.php file for some extras.

### Theme Menu Locations

This theme adds menu support and the following locaitons:

- Primary Menu
- Secondary Menu
- Footer Menu