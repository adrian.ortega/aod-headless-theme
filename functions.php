<?php

add_action('after_setup_theme', function () {
    add_theme_support('title-tag');
    add_theme_support('post-thumbnails');
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);
    add_theme_support('align-wide');
    add_theme_support('customize-selective-refresh-widgets');
});

add_filter('excerpt_more', function () {
    return '&hellip;';
});

add_shortcode('current_year', function () {
    return date('Y');
});

register_nav_menus([
    'primary_menu'   => __('Primary Menu', 'aod'),
    'secondary_menu' => __('Secondary Menu', 'aod'),
    'footer_menu'    => __('Footer Menu', 'aod'),
]);
