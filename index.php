<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="https://aortegadesign.com/favicon.ico" type="image/x-icon">
    <link rel="icon" href="https://aortegadesign.com/favicon.ico" type="image/x-icon">
    <meta name="robots" content="noindex,nofollow">
    <title>Oops | Adrian Ortega</title>
    <style type="text/css">
        @import 'https://fonts.googleapis.com/css?family=Asap:400,400i,700,700i|Signika+Negative:600|Quicksand:700';/*! normalize-scss | MIT/GPLv2 License | bit.ly/normalize-scss */html{font-family:"Asap", sans-serif;line-height:1.15;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}body{margin:0}article,aside,footer,header,nav,section{display:block}h1{font-size:2em;margin:0.67em 0}figcaption,figure{display:block}figure{margin:1em 40px}hr{box-sizing:content-box;height:0;overflow:visible}main{display:block}pre{font-family:monospace, monospace;font-size:1em}a{background-color:transparent;-webkit-text-decoration-skip:objects}abbr[title]{border-bottom:none;text-decoration:underline;text-decoration:underline dotted}b,strong{font-weight:inherit}b,strong{font-weight:bolder}code,kbd,samp{font-family:monospace, monospace;font-size:1em}dfn{font-style:italic}mark{background-color:#ff0;color:#000}small{font-size:80%}sub,sup{font-size:75%;line-height:0;position:relative;vertical-align:baseline}sub{bottom:-0.25em}sup{top:-0.5em}audio,video{display:inline-block}audio:not([controls]){display:none;height:0}img{border-style:none}svg:not(:root){overflow:hidden}button,input,optgroup,select,textarea{font-family:"Asap", sans-serif;font-size:100%;line-height:1.15;margin:0}button{overflow:visible}button,select{text-transform:none}button,html [type="button"],[type="reset"],[type="submit"]{-webkit-appearance:button}button::-moz-focus-inner,[type="button"]::-moz-focus-inner,[type="reset"]::-moz-focus-inner,[type="submit"]::-moz-focus-inner{border-style:none;padding:0}button:-moz-focusring,[type="button"]:-moz-focusring,[type="reset"]:-moz-focusring,[type="submit"]:-moz-focusring{outline:1px dotted ButtonText}input{overflow:visible}[type="checkbox"],[type="radio"]{box-sizing:border-box;padding:0}[type="number"]::-webkit-inner-spin-button,[type="number"]::-webkit-outer-spin-button{height:auto}[type="search"]{-webkit-appearance:textfield;outline-offset:-2px}[type="search"]::-webkit-search-cancel-button,[type="search"]::-webkit-search-decoration{-webkit-appearance:none}::-webkit-file-upload-button{-webkit-appearance:button;font:inherit}fieldset{padding:0.35em 0.75em 0.625em}legend{box-sizing:border-box;display:table;max-width:100%;padding:0;color:inherit;white-space:normal}progress{display:inline-block;vertical-align:baseline}textarea{overflow:auto}details{display:block}summary{display:list-item}menu{display:block}canvas{display:inline-block}template{display:none}[hidden]{display:none}*,*:before,*:after{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}body{background:#f5f5f5}.logo{display:inline-block;font-family:"Quicksand", sans-serif;font-size:32px;text-transform:lowercase;text-align:center;letter-spacing:-1px}.logo:before,.logo:after{content:" ";display:table}.logo:after{clear:left}.logo-wrapper{text-align:center;margin-top:20px;margin-bottom:40px}.logo__a,.logo__o{display:inline-block}.logo__a{color:#021c35}.logo__o{color:#029eb6}.container{margin:0 auto;max-width:1000px;padding-left:10px;padding-right:10px}.projects{padding:0 5px;background:#fff;margin:0 0 10px;box-shadow:0 1px 4px #d9d9d9}.projects__title{background:#021c35;color:#fff;margin-top:0;margin-bottom:0;display:block;position:relative;padding:0 20px;height:48px;line-height:48px}.projects__toggle-button{display:block;position:absolute;top:0;right:0;width:48px;height:48px;text-align:center;font-size:24px;cursor:pointer}.projects__toggle-button:after{content:"\25be"}.projects__toggle{display:none}.projects__toggle:checked ~ .projects__list,.projects__toggle:checked ~ .projects__headers{display:block}.projects__toggle:checked ~ .projects__title .projects__toggle-button:after{content:"\00D7"}.projects__title,.projects__headers{margin-right:-5px;margin-left:-5px}.projects__headers{font-size:12px;text-transform:uppercase;font-weight:bold;background:#deeccf;border-bottom:1px solid #bedaa0;margin-bottom:10px}.projects__headers:before,.projects__headers:after{content:" ";display:table}.projects__headers:after{clear:left}.projects__header{float:left;padding:7.5px;color:#435259}.projects__header--type{width:64px;text-align:center}.projects__header--modified{float:right;width:165px;padding-left:0;padding-right:0}.projects__headers,.projects__list{display:none}.projects .project:before,.projects .project:after{content:" ";display:table}.projects .project:after{clear:left}.projects .project:not(:last-child){border-bottom:1px solid #d9d9d9}.projects .project:nth-child(even){background:#f5f5f5}.projects .project a{display:block;position:relative;padding:10px 20px 13.5px 64px;text-decoration:none;color:#021c35}.projects .project__icon{display:inline-block;width:32px;height:32px;-webkit-border-radius:50%;-moz-border-radius:50%;border-radius:50%;position:absolute;top:5px;left:15px;color:#e93564;font-size:9px;font-weight:bold;text-transform:uppercase;line-height:30px;text-align:center;border:2px solid #e93564}.projects .project__icon--html{border-color:#f16529;color:#f16529}.projects .project__icon--html:after{content:"html"}.projects .project__icon--php{border-color:#ad35e9;color:#ad35e9}.projects .project__icon--php:after{content:"php"}.projects .project__icon--wordpress{border-color:#21759b;color:#21759b}.projects .project__icon--wordpress:after{content:"wp"}.projects .project__icon--magento{border-color:#f26322;color:#f26322}.projects .project__icon--magento:after{content:"mage"}.projects .project__date{float:right;color:#6a8a9a;font-size:14px;width:140px}.projects .project:hover .project__name{font-weight:bold}.projects .project:hover .project__icon{color:#fff}.projects .project:hover .project__icon--php{background:#ad35e9}.projects .project:hover .project__icon--html{background:#f16529}.projects .project:hover .project__icon--wordpress{background:#21759b}.projects .project:hover .project__icon--magento{background:#f26322}.main__footer{padding:20px 0;text-align:center}.main__footer a{font-weight:bold;color:#021c35;text-decoration:none}
    </style>
</head>
<body>
<header class="main__header logo-wrapper">
    <a href="http://aortegadesign.com" class="logo">
        <span class="logo__a">Adrian</span><span class="logo__o">Ortega</span>
    </a>
</header>
<div class="main-container">
    <div style="width:100%;max-width:600px;margin:100px auto 200px;text-align:center;">
        <h1>¯\_(ツ)_/¯<br><br>
            Sorry, there's nothing here.</h1>
        <h3 style="color:rgba(0,0,0,0.4)">Someday, I'm sure this will be amazing.</h3>
    </div>

</div>
<footer class="main__footer">
    <p>
        <small>
            &copy; <?php echo date('Y') ?> Copyright
            <a href="mailto:hello@aortegadesign.com">Adrian Ortega</a>, All rights reserved.
            <a href="https://aortegadesign.com/fine-print" title="The fine print">Fine Print</a> |
            <a href="https://aortegadesign.com/privacy-policy" title="Privacy policy">Privacy Policy</a>
        </small>
    </p>
</footer>
</body>
</html>
